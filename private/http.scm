;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018, 2019 Gwen Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; HTTP client using a subprocess.

(library (akku private http)
  (export
    download-file
    upload-file)
  (import
    (rnrs (6))
    (akku lib utils)
    (akku private compat)
    (akku private logging))

(define logger:akku.http (make-logger logger:akku 'http))
(define log/info (make-fmt-log logger:akku.http 'info))
(define log/error (make-fmt-log logger:akku.http 'error))
(define log/warn (make-fmt-log logger:akku.http 'warning))
(define log/debug (make-fmt-log logger:akku.http 'debug))
(define log/trace (make-fmt-log logger:akku.http 'trace))

(define (download-file url local-filename)
  (when (enum-set-member? (setting no-network) (get-settings))
    (error 'download-file "Networking disabled; refusing to download"
           url local-filename))
  (log/trace "Downloading " url " to " local-filename)
  (putenv "AKKU_OPT" (if (memq (get-log-threshold) '(debug trace))
                         "-v" ""))
  (putenv "AKKU_URL" url)
  (putenv "AKKU_FN" local-filename)
  (run-command "curl -sSL $AKKU_OPT \"$AKKU_URL\" -o \"$AKKU_FN\""))

(define (upload-file filename url)
  (when (enum-set-member? (setting no-network) (get-settings))
    (error 'upload-file "Networking disabled; refusing to upload"
           filename url))
  (log/trace "Uploading " filename " to " url)
  (putenv "AKKU_OPT" (if (memq (get-log-threshold) '(debug trace))
                         "-v" ""))
  (putenv "AKKU_URL" url)
  (putenv "AKKU_FN" filename)
  (run-command "curl -sS $AKKU_OPT --upload-file \"$AKKU_FN\" \"$AKKU_URL\"")))
